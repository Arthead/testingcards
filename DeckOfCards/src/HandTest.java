import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class HandTest {
	Hand myHand;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		myHand = new Hand();
	}

	@Test
	public void testAdd() {
		Card c = new Card("Spades", 3);
		myHand.add(c);
		assertEquals(c.toString()+"\n", myHand.toString());
	}
	@Test
	public void testAdd2() {
		Card c = new Card("Spades", 3);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		assertEquals(5, myHand.getCardsOnHand());
	}

	@Test
	public void testPlayCard() {
		Card c = new Card("Spades", 3);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		Card playedCard = myHand.playCard(0);
		assertEquals(c.toString(), playedCard.toString());
	}

	@Test
	public void testPlayCard2() {
		Card c = new Card("Spades", 3);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		myHand.add(c);
		Card playedCard = myHand.playCard(0);
		playedCard = myHand.playCard(4);
		assertNull(playedCard);
	}	
	
}
