
public class Card {
	String color;
	int value;
	
	public Card(String color, int value) {
		this.color = color;
		this.value = value;
	}
	
	public String toString(){
		return value + " of " + color;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

}
