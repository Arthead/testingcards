import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Deck {
	private List<Card> theDeck;

	public Deck(){
		init();
	}
	
	public void init(){
		theDeck = new ArrayList<Card>();
		String[] colors = {"Hearts", "Clubs", "Diamonds","Spades"};
		for(String color : colors){
			for(int value = 1; value <= 13; value++){
				Card card = new Card(color,value);
				theDeck.add(card);
			}
		}
	}
	
	public void shuffle(){
		Random rnd = new Random();
		//N�r vi testar anv�nd raden nedan ist�llet f�r raden ovanf�r
		//MyRandom rnd = new MyRandom();
		for(int i = 0; i < 500; i++){
			int first = rnd.nextInt(52);
			int second = rnd.nextInt(52);
			Collections.swap(theDeck, first, second);
		}
	}
	
	public String toString(){
		String theString = "";
		for(Card card : theDeck){
			theString += card.toString() + "\n";
		}
		return theString;
	}
	
	public int getCardsLeft(){
		return theDeck.size();
	}
	
	public Card deal(){
		Card card = null;
		if(theDeck.size() > 0){
			card = theDeck.remove(0);
		}
		return card;
	}
	
	public List<Card> getDeck(){
		return theDeck;
	}
}
