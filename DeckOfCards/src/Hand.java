import java.util.ArrayList;
import java.util.List;


public class Hand {
	public static final int MAX_CARDS = 5;
	List<Card> theHand;
	int cardsOnHand;
	
	public Hand(){
		theHand = new ArrayList<Card>();
		cardsOnHand = 0;
	}
	
	
	public int getCardsOnHand(){
		return cardsOnHand;
	}
	
	public void add(Card c){
		if(cardsOnHand < MAX_CARDS){
			theHand.add(c);
			cardsOnHand++;
		}
	}
	
	public Card playCard(int i){
		Card c = null;
		if(i >= 0 && i < cardsOnHand){
			c = theHand.remove(i);
			cardsOnHand--;
		}
		return c;
	}
	
	//Eftersom vi anv�nder en lambda i metoden kan inte hand vara
	//deklarerad som en lokal variabel eftersom den d� m�ste vara
	//final och vi kan d� inte tilldela den ett nytt v�rde.
	//Vi l�gger den h�r ist�llet som en klassvariabel fast den h�r 
	//till den h�r metoden
	private String hand = "";
	public String toString(){
		if(theHand != null && cardsOnHand >= 0){
			theHand.stream().forEach(card -> hand += card.toString() + "\n");
		}
		return hand;
	}
}
