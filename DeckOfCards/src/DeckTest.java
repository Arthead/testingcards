import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class DeckTest {
	Deck myDeck;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		myDeck = new Deck();
	}

	@Test
	public void testShuffle() {
		myDeck.shuffle();
		Deck secondDeck = new Deck();
		secondDeck.shuffle();
		String s1 = myDeck.toString();
		String s2 = secondDeck.toString();
		assertEquals(s1, s2);
	}


	@Test
	public void testGetCardsLeft() {
		assertEquals(52, myDeck.getCardsLeft());;
	}

	@Test
	public void testDeal() {
		Card c = myDeck.deal();
		if(!c.toString().equals("1 of Hearts")){
			fail("Expected 1 of Hearts got " + c.toString());
		}
		for(int i = 0; i < 51; i++){
			if(myDeck.deal() == null){
				fail("Expected card. Got null at " + i);
			}
		}
		c = myDeck.deal();
		assertNull(c);
	}


	@Test
	public void testInit() {
		String[] colors = {"Hearts", "Clubs", "Diamonds","Spades"};
		
		for(String color : colors){
			for(int i = 1; i <= 13; i++){
				Card c = myDeck.deal();
				if(c.getColor() != color || c.getValue() != i){
					fail("Wrong card. Expected " + c.toString() + " got " + i + " of " + color);
				}
			}
		}
	}	
}
